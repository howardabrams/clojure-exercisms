(ns run-length-encoding
  (:require [clojure.string :refer [join]]))

(defn encode-sequence
  "Helper function for `run-length-encode` that takes a string of a repeating
  letter, and returns the number of repeats and the letter... unless the number
  of letters is 1, and then it just returns that letter."
  [enty]
  (let [cnt (count enty)]
    (if (= cnt 1)
      enty
      (format "%d%s" cnt (subs enty 0 1)))))

(defn run-length-encode
  "encodes a string with run-length-encoding"
  [s]
  (->> s                        ; Assuming s is "aaaaabcc", then...
       (re-seq #"([A-z ])\1*")  ; (["aaaaa" "a"] ["b" "b"] ["cc" "c"])
       (map first)              ; ("aaaaa" "b" "cc")
       (map encode-sequence)    ; ("5a" "b" "2c")
       (join)))                 ; "5ab2c"


(defn analyze-start
  "Given the results of our regular expression, either a single letter and two
  nils, or the original code and the integer and number, and returns the expect
  number of letters."
  [[single-letter times letter]]
  ;; We either have a single letter
  (if letter
    (repeat (Integer/parseInt times) letter)
    single-letter))

(defn run-length-decode
  "decodes a run-length-encoded string"
  [s]
  (let [regex #"([0-9]+)([A-z ])|[A-z ]"]

    ;; The regular expression splits the string into tuples of a single
    ;; letter (if we should only do one of that letter), or a number and its
    ;; letter. We then expand that tuple into a series of letters of the correct
    ;; length (with analyze-start).

    (->> s                   ; Assume we have "8xy2z"
         (re-seq regex)      ;  to (["8x" "8" "x"] ["y" nil nil] ["2z" "2" "z"])
         (map analyze-start) ;  to (("x" "x" "x" "x" "x" "x" "x" "x") "y" ("z" "z"))
         flatten             ;  to ("x" "x" "x" "x" "x" "x" "x" "x" "y" "z" "z")
         join)))             ;  to "xxxxxxxxyzz"
