(ns nucleotide-count
  (:refer-clojure :exclude [count]))

(defn count
  "Given a molecular letter, count the occurances in the strand."
  [mole strand]
  (when-not (#{\A \C \T \G} mole)
          (throw (Exception. "invalid nucleotide")))
  ((frequencies strand) mole 0))

(defn nucleotide-counts
  "Given a string, returns the signature pattern, where each
   molecule is a key, and the number of occurances is the value."
  [strand]
  (merge {\A 0, \T 0, \C 0, \G 0} (frequencies strand)))
