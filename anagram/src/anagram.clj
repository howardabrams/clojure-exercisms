(ns anagram
  (:require [clojure.string :refer [lower-case]]))

(defn compare-anagrams
  "Predicate compares parameters to decide is word1 is an anagram of word2.
   Does that with the 'frequencies' function that creates a word's anagram pattern."
  [word1 word2]
  (if (not (.equalsIgnoreCase word1 word2))
    (= (-> word1
           lower-case
           frequencies)
       (-> word2
           lower-case
           frequencies))))

(defn anagrams-for
  "Given a string and a list of words, return words that are anagrams of the first"
  [a-word words]
  (filter #(compare-anagrams a-word %) words))
