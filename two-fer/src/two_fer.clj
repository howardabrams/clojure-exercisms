(ns two-fer)

(defn two-fer
  ([]     (two-fer "you"))
  ([name] (format "One for %s, one for me." name)))

;; Another approach (which is normally not cleaner or clearer) is convert the
;; parameter-to-list feature, and destructure it to a variable. I agree with
;; Stuart on this...
;; See https://stuartsierra.com/2015/06/01/clojure-donts-optional-arguments-with-varargs
;;
;; (defn two-fer [& [name]]
;;   (format "One for %s, one for me."
;;           (or name "you")))
