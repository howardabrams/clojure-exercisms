(ns robot-name)

;; This is one way to create an array of uppercase letters. ;-)
(def letters (map char (range 65 91)))

(defn seeder
  "Return a seeding value of 'some sort'"
  []
  (* (rand) (.getTime (java.util.Date.))))

(defn robot
  "Return a robot 'object', with is just a Clojure reference to a
  seeding value that will be used to return the name."
  []
  (ref (seeder)))

(defn robot-name
  "Names a robot based on some seed name"
  [robot]
  (let [r (java.util.Random. @robot)

        ;; A couple of helper functions:
        new-letter #(nth letters (.nextInt r 26))
        new-number #(.nextInt r 10)

        word-part (->> new-letter
                       repeatedly
                       (take 2)
                       (apply str))
        nums-part (->> new-number
                       repeatedly
                       (take 3)
                       (apply str))]
    (str word-part nums-part)))

(defn reset-name
  "Resets the name for generating a new name. We set a new seeder
  transactionally."
  [robot]
  (dosync (ref-set robot (seeder))))
