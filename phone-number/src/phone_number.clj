(ns phone-number
  (:require [clojure.string :refer [join replace blank?]
                            :rename {replace re-replace}]))

(defn phone-number-parser
  "Returns a phone number as its parts, i.e. country code, area code,
  prefix and suffix. Returns nil if the phone number is invalid."
  [num-as-str]
  (-> num-as-str
           (re-replace #"[^0-9]" "")
      (->> (re-matcher #"(1)?(\d*)?(\d{3})(\d{3})(\d{4})$"))
            re-find
            rest))   ;; Return only the regexp groups

(defn phone-number-logic
  "Returns the validated parts of a phone number, with 000 returned
  for invalid parts. Any initial '1', while required to specified for
  the country code, is not returned."
  [num-as-str]
  (let [default ["" "000" "000" "0000"]
        parts   (phone-number-parser num-as-str)
        intl    (nth parts 0 nil)
        country (nth parts 1 "")]
    (cond
      (and (not (blank? country)) (= "1" intl))    (rest parts)
      (and (blank? country) (not (empty? parts)))  (rest parts)
      :otherwise                                    default)))

(defn number
  "Return a compressed, but validate phone number, e.g.
   18881234567 => 8881234567"
  [num-as-str]
  (-> num-as-str
      phone-number-logic
      join))

(defn area-code
  "Return the area code of the given phone number."
  [num-as-str]
  (nth (phone-number-logic num-as-str) 1))

(defn pretty-print
  "Prints a phone number in a standard format."
  [num-as-str]
  (let [ [country area prefix suffix]
                    (phone-number-logic num-as-str)]
    (if (not (blank? country))
      (format "1-%s (%s) %s-%s" country area prefix suffix)
      (format      "(%s) %s-%s"         area prefix suffix))))
