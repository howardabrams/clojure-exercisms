(ns bob
  (:require [clojure.string :as str]))

(defn response-for
  "Return message based on the input string, `msg`."
  [msg]
  (let [ops-msg (-> msg
                   (str/replace #"[0-9,.%^@#$*\(\)]" "")
                   str/trim)]
    (cond
      (re-matches #"^ *$"                   msg) "Fine. Be that way!"
      (re-matches #"^[A-Z][A-Z' ?+!]+$" ops-msg) "Whoa, chill out!"
      (re-matches #".*\?$"              ops-msg) "Sure."
      :default                                   "Whatever.")))
