(ns rna-transcription
  (:require [clojure.string :as str]))

(defn dna->rna
  "Given a single letter in a DNA, returns equivalent RNA.
   If given an invalid DNA letter, this throws Assertion Error"
  [dna-letter]
  (let [encoding   {\G \C
                    \C \G
                    \T \A
                    \A \U}
        rna-letter (encoding dna-letter)]
    (or rna-letter
        (assert rna-letter
                (format "Invalid sequence letter: %c" dna-letter)))))

(defn to-rna
  "Given a DNA strand, as a string a letters, return its RNA Complement."
  [strand]
  (->> strand
       (map dna->rna)
       str/join))
