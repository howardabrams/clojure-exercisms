(ns reverse-string
  (:require [clojure.string :refer [join]]))

(defn reverse-string [s]
  (let [backwards (reverse s)]
    (join "" backwards)))
