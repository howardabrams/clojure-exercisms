(ns beer-song
  (:require [clojure.string :as str]
            [clojure.test :refer [is with-test]]))

;; Begin with a few simple helper functions to make the
;; algorithm in the `verse' easier to read.

(defn plural? [n] (not (= n 1)))

(defn plural [num thing things]
  (if (plural? num) things thing))

(defn bottles [num] (plural num "bottle" "bottles"))
(defn one-it  [num] (plural num "it" "one"))

(defn int->str [num]
  (if (zero? num) "no more" (str num)))

(defn verse
  "Take one down... pass it around"
  [current]
  (let [next-round  (dec current)
        current-msg (int->str current)
        next-msg    (int->str next-round)
        these-bottles (bottles current)
        next-bottles (bottles next-round)]

    ;; The zeroth case is so extreme, what with the store
    ;; closing at 10pm right in the middle of the 45th verse,
    ;; so we'll just return the appropriate ending:
    (if (zero? current)
      "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n"
      (format "%s %s of beer on the wall, %s %s of beer.
Take %s down and pass it around, %s %s of beer on the wall.
"             current-msg these-bottles
              current-msg these-bottles
              (one-it current)
              next-msg next-bottles))))

(defn sing
  "Let's sing one or more verses... until we're hoarse."
  ([all-verses]  (sing all-verses 0))
  ([start end]
   (str/join "\n"
             (loop [start start
                    end   end
                    coll  []]
               (if (< start end)     ; Soon as we are out
                 coll                ; Return back our verses
                 (recur (dec start)  ; Otherwise, combine verse
                         end         ; with the next one...
                        (conj coll (verse start))))))))
