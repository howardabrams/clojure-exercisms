(ns grade-school)

(defn add
  "Add a student to a school database"
  [db student grade]
  (let [students     (get db grade [])
        new-students (conj students student)]
    (assoc db grade new-students)))

(defn grade
  "Return the students in a grade"
  [db grade]
  (db grade []))

(defn sorted
  "Return a doubly-sorted map"
  [db]
  (into (sorted-map) db))
