(ns armstrong-numbers)

(defn armstrong? [n]
  ;; To convert a number into a list of digits, we convert to string of ascii
  ;; characters, and use Java's Character class to convert to an integer.
  ;; See https://docs.oracle.com/javase/7/docs/api/java/lang/Character.html#digit(char,%20int)
  (let [digits (map #(Character/digit % 10) (str n))
        exp    (count digits)]
    (== n
       (reduce #(+ %1 (Math/pow %2 exp)) 0 digits))))
