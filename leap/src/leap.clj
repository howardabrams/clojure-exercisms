(ns leap)

(defn leap-year?
  "Predicate is true if year given is a leap year.
   While not the most succinct, it is quite readable."
  [year]
  (let [divisible? #(= 0 (mod year %))]
    (cond
      (divisible? 400) true
      (divisible? 100) false
      (divisible? 4)   true
      :otherwise       false)))
