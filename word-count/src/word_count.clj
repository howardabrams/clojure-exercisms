(ns word-count
  (:require [clojure.string :as str]))

(defn count-word
  "Helper reducing function returns a map counting the previous
  results as well as the current, updated count of `word`."
  [mmap word]
  (assoc mmap word (inc (mmap word 0))))

(defn convert-word
  "Takes a token word and lower-cases it and strips out the
  punctuation and other symbols."
  [word]
  (-> word
      (str/replace #"[:,.!^@$%&]" "")
      str/lower-case))

(defn empties [string] (not (str/blank? string)))

(defn word-count
  "Given a string of words, return a map of unique words and the
  number of times they occur"
  [string-of-words]
  (let [tokens (str/split string-of-words #"\s")]
    (->> tokens
         (map convert-word)
         (filter empties)
         (reduce count-word {}))))
