(ns hamming)

(defn not-matched?
  "Helper function that unpacks and compares a tuple."
  [[a b]]
  (not (= a b)))

(defn distance [strand1 strand2] ; <- arglist goes here
  "Calculates the number of differences in the two DNA strands."
  [strand1 strand2]
  ;; Make sure that the strand lengths are the same, otherwise, we are
  ;; undefined, and want to return nil:
  (when (= (count strand1) (count strand2))

    (->> (interleave strand1 strand2)
         (partition 2)
         (filter not-matched?)
         count)))
