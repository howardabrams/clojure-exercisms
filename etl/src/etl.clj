(ns etl
  (:require [clojure.string :refer [lower-case]]))

(defn flip-map-entry
  "Given a key and a vector of values, create a map with each value is
  a key and the key is the value. For instance:
  (flip-map-entry 2 [:a :b :c]) => {:c 2, :b 2, :a 2}
  Oh, and did I mention we lowercase each key?"
  [old-key new-key-list]
  (letfn [(flipper [new-key] (hash-map (lower-case new-key) old-key))]
    (->> new-key-list
         (map flipper)    ;; Create a bunch of flipped hashes
         (apply merge)))) ;; ... and squash 'em

(defn transform
  "Transforms the given map by taking each key-value pair, flipping it
  into a map of values-to-keys, and merging them."
  [scores]
  (letfn [(flip-each [m k v]
            (merge m (flip-map-entry k v)))]
    (reduce-kv flip-each {} scores)))
