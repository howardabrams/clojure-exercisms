(ns meetup
  (:import java.util.Calendar))

;; The unwritten goals for this project (well, I guess they are
;; now written) is to not do any shortcuts with Joda date-time
;; library, but just get the basics from the Java Calendar class.
;;
;; The difference between using Java and a Clojure-wrapper
;; library around Joda is significant. For instance, getting
;; the day of the week for a particular date using Calendar is:
;;
;; (let [date (doto (Calendar/getInstance)
;;              (.set (Calendar/YEAR) year)
;;              (.set (Calendar/MONTH) (dec month))
;;              (.set (Calendar/DAY_OF_MONTH) day))]
;;   (.get date (Calendar/DAY_OF_WEEK))))
;;
;; If we were to use Joda, then the above code would be:
;;
;; (-> (t/date-time year month day)
;;      t/day-of-week)

(defn get-date
  "Return a day as a Calendar object."
  [year month day]
  (doto (Calendar/getInstance)
               (.set (Calendar/YEAR) year)
               (.set (Calendar/MONTH) (dec month))
               (.set (Calendar/DAY_OF_MONTH) day)))

(defn get-lastday
  "Return the last day of a month as an integer."
  [year month]
  (let [date (get-date year month 1)]
    (.getActualMaximum date (Calendar/DAY_OF_MONTH))))

(defn weekday?
  "Predicate is true if date given matches a day of the week."
  [weekday date]
  (let [date-weekday (.get date (Calendar/DAY_OF_WEEK))]
    (= date-weekday weekday)))

(defn get-weekdays
  "Returns all specified weekdays in a particular month."
  [year month weekday]
  (let [convert-to-date (partial get-date year month)
        wanted-weekday  (partial weekday? weekday)]

    (->> (get-lastday year month)
          inc                         ;; Since range is exclusive...
         (range 1)
         (map convert-to-date)
         (filter wanted-weekday))))

;; Since the last parameter to `meetup` essentially dictates
;; which of the three method signatures we will try, this seems
;; like a nice approach to create a `defmulti` dispatcher:

(defmulti meetup-dispatch (fn [type month year weekday] type))

(defmethod meetup-dispatch :last [_ month year weekday]
  (last (get-weekdays year month weekday)))

(defmethod meetup-dispatch :teenth [_ month year weekday]
  ;; The teenth dispatch function requires a filter based on a
  ;; value and not on its position in the returned values:
  (letfn [(day-of  [date] (.get date (Calendar/DAY_OF_MONTH)))
          (teenth? [date] (and (>= (day-of date) 13)
                               (<= (day-of date) 19)))]
    (first (filter teenth? (get-weekdays year month weekday)))))

;; The :default is called if the other symbols didn't match.
;; This converts the symbol to a number, and uses it for the nth.

(defmethod meetup-dispatch :default [weeknum month year weekday]
  (let [week ({:first   0
               :second  1
               :third   2
               :fourth  3} weeknum)]
    (nth (get-weekdays year month weekday) week)))

(defn meetup
  "Returns a year month day vector based on the best day that matches
  the symbols for the given."
  [month year weekday weeknum]
  (let [weekday-num ({:sunday    1
                      :monday    2
                      :tuesday   3
                      :wednesday 4
                      :thursday  5
                      :friday    6
                      :saturday  7} weekday)
        best-date (meetup-dispatch weeknum month year weekday-num)]
    [year month (.get best-date (Calendar/DAY_OF_MONTH))]))
